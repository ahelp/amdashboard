﻿Imports System.Data.SqlClient
Imports System.Web.Services

Public Class Dashboard
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
    End Sub

    
    ' maybe make this an extension
    Private Shared ReadOnly DatetimeMinTimeTicks As Long = New DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks

    Private Shared Function DateAsMilliseconds(input As DateTime) As Long
        Dim x = (input.ToUniversalTime().Ticks - DatetimeMinTimeTicks) / 10000
        Return x
    End Function


    <WebMethod()>
    Public Shared Function Last7Days() As List(Of AMScore)
        Dim scores = New List(Of AMScore)
        Dim view As String = "vw_AMS_ResultsLast7"
        scores = GetScores(view)
        Return scores
    End Function

    Private Shared Function GetScores(ByVal view As String) As List(Of AMScore)
        Dim Query As String = "SELECT * FROM " & view
        Dim results = New List(Of AMScore)
        Dim intScoreTotal As Integer = 0
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("JBMOMConnectionString").ToString()
            Using cmd As New SqlCommand(Query, conn)
                conn.Open()
                Using rdr = cmd.ExecuteReader()
                    While rdr.Read
                        Dim score = New AMScore
                        score.Name = rdr("SlsServiceRep")
                        score.Score = rdr("Score")
                        intScoreTotal += rdr("Score")
                        results.Add(score)
                    End While
                    If Not rdr.IsClosed Then
                        rdr.Close()
                    End If
                End Using
            End Using
        End Using

        For Each score As AMScore In results
            score.TotalAverage = intScoreTotal / results.Count
        Next
        Return results
    End Function


    Private Shared Function MintuesToHours(minutes As Integer) As String

        Dim h As Integer = minutes \ 60
        Dim m As Integer = minutes - (h * 60)
        'Dim hours As String = CType(h, String) & ":" & CType(m, String)

        Dim hours = h.ToString("00") & ":" & m.ToString("00")

        'label1.Text = timeElapsed
        'Dim hours = New DateTime(1, 1, 1, 0, minutes, 0)
        Return hours
    End Function

    
    Private Shared Function GetDoubleValue(column As String, reader As IDataRecord) As Double
        If reader.IsDBNull(reader.GetOrdinal(column)) Then
            Return 0
        End If
        Return reader.GetDouble(reader.GetOrdinal(column))
    End Function

    Private Shared Function ParseCharToInt(input As Char) As Integer
        Dim output = 0

        If Integer.TryParse(input.ToString(), output) Then
            Return output
        End If
        Return 0
    End Function
End Class

Public Class AMScore
    Public Property Name As String
    Public Property Score As Integer
    Public ReadOnly Property Average As Integer
        Get
            Return Score / 7
        End Get
    End Property
    Public Property TotalAverage As Integer
End Class


