﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Dashboard.aspx.vb" Inherits="AMDashboard.Dashboard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!-- ************************************************************************* -->
<!--
This page was developed as a proof of concept.

It functions and was nearly code complete when the project changed directions.
It's likely at some point this will be revisited so I'm leaving the page
active and intact, but it has not been tested or verified.

There is a good deal of refactoring and cleanup needed. But I will leave
that for the next round of requirements.

There is a lot of duplication between this page and status-dashboard/dashboard.aspx

Tony Maddox 2017-02-02
-->
<!-- ************************************************************************* -->


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>OEE Dashboard</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" 
    href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css?parameter=1" 
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" 
    crossorigin="anonymous" />

<link href="../css/dashboard.css" rel="stylesheet" type="text/css"/>

<style>
   

   
    /* Watermark */
    html:after {

        content: "Prototype";
        font-size: 950%;
        color: rgba(0, 0, 0, .05);
        z-index: 9999;
        cursor: default;
        display: block;
        position: fixed;
        top: 25%;
        right: 0;
        bottom: 0;
        left: 15%;
        font-family: sans-serif;
        font-weight: bold;
        font-style: italic;
        text-align: center;
        line-height: 100%;

        -webkit-pointer-events: none;
        -moz-pointer-events: none;
        -ms-pointer-events: none;
        -o-pointer-events: none;
        pointer-events: none;

        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        transform: rotate(-45deg);

        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
        user-select: none;
    }
    
    
    
/*#oee-gauge > table > tbody > tr > td:nth-child(1) > div > div:nth-child(1) > div > svg > g > circle:nth-child(2) {fill: #5cb85c;}*/
    </style>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    $(document)
        .ready(function() {
            google.charts.load("current", { packages: ["corechart", "gauge", "bar"] });
            google.charts.setOnLoadCallback(drawVisualizations);

        });

    function drawVisualizations() {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: 'dashboard.aspx/Last7Days',
            data: '{}',
            success:
                function (response) {
                    var data = response.d;

                    // ytd OEE info
                    last7DaysCharts(data);


                },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ytd err');
            }
        });
    }

    function getLast7Days() {
        var promise = $.ajax({
            type: 'POST',
            dataType: 'json',
            async: false,
            cache: false,
            contentType: 'application/json',
            url: 'dashboard.aspx/Last7Days',
            data: '{}'
        });

        return promise;
    }
   

    function last7DaysCharts(input) {
        var data = formatLast7DaysGraph(input);
        drawLast7DaysGraph(data)
    }

    function drawLast7DaysGraph(data) {
        
        var options = {
            hAxis: { gridlines: { count: 12, color: '#ffffff' }, slantedText:true, slantedTextAngle: 45 },
            legend: 'none',
            //animation: { 'startup': true, duration: 1000, easing: 'out' },
            seriesType: 'bars',
            series: { 1: { type: 'line', color: '#5bc0de', lineWidth: 5} },
            chartArea: { 'width': '100%', height: '50%' },
            reverseCategories: true,
            colors: ['#337ab7', '#d95f02', '#7570b3']
        };

        var chart = new google.visualization.ComboChart(document.getElementById('ams-last7'));
        chart.draw(data, options);


        setInterval(function () {
            var promise = getLast7Days();
            promise.then(function (data) {
                var d = data.d;

                data = formatLast7Days(input);
                chart.draw(data, options);

            },
                    function (data) {
                        console.log('err happened!');
                    });

        },
            180000);

    }

    
    function formatLast7DaysTable(data) {
        var table = new google.visualization.DataTable();

        table.addColumn('string', 'Name');
        table.addColumn('number', 'Score');
        table.addColumn('number', 'Average');

        $.each(data,
            function (key, value) {
                table.addRow([
                    value.Name , value.Score, value.Average
                ]);
            });
        return table;
    }

    function formatLast7DaysGraph(data) {
        var table = new google.visualization.DataTable();

        table.addColumn('string', 'Name');
        table.addColumn('number', 'Score');
        table.addColumn('number', 'TotalAverage');

        $.each(data,
            function (key, value) {
                table.addRow([
                    value.Name, value.Score, value.TotalAverage
                ]);
            });
        return table;
    }
    
    function centerGauge(gaugeName) {
        var id = "#" + gaugeName + " table";
        var y = $(id);
        y.removeAttr("style");
    }
</script>
</head>
<body cz-shortcut-listen="true" style="padding: 0">

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4 col-sm-push-4">
            <h4>Last 7 Days</h4>
            <div id="ams-last7" style="height:400px;"></div>
        </div>
    </div>
</div>

<!-- Latest compiled and minified JavaScript -->
<script 
    src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" 
    crossorigin="anonymous"></script>

</body>


</html>